# TestBoards



## Description

FRC Team 7770, Infinite Voltage, uses test boards to teach wiring and programming skills.  The board consists of a RoboRio, power ditribution hub (PDH), low voltage module, robot signal light (RSL), circuit breaker, and a mix of motors, controllers, and sensors.

## Getting started

Clone the code
```
git clone https://gitlab.com/frc7770/testboards.git
```
