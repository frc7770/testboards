// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;


import com.ctre.phoenix.sensors.PigeonIMU;
import com.studica.frc.AHRS;
import com.studica.frc.AHRS.NavXComType;

import edu.wpi.first.util.sendable.SendableRegistry;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.leds.AllianceColor;
import frc.robot.leds.LEDSubsystem;
import frc.robot.leds.NewLEDSubsystem;
import frc.robot.leds.Rainbow;
import frc.robot.motors.ButtonVictorSPXCommand;
import frc.robot.motors.SparkFlexSubsystem;
import frc.robot.motors.SparkMaxSubsystem;
import frc.robot.motors.TalonFXSubsystem;
import frc.robot.motors.TalonSRXSubsystem;
import frc.robot.motors.VictorSPXSubsystem;
import frc.robot.motors.XboxSparkMaxCommand;
import frc.robot.motors.XboxTalonFXCommand;
import frc.robot.motors.XboxTalonSRXCommand;
import frc.robot.motors.XboxVictorSPXCommand;
import frc.robot.sensors.ColorSensor;
import frc.robot.sensors.LinearColorEncoder;
import frc.robot.servos.ServoCommand;
import frc.robot.servos.ServoSubsystem;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends TimedRobot {

  // Xbox Controller
  public static final CommandXboxController driverXbox = new CommandXboxController(0);
  // Test motors
  public static final SparkMaxSubsystem sparkMaxSubsystem = new SparkMaxSubsystem();
  public static final SparkFlexSubsystem sparkFlexSubsystem = new SparkFlexSubsystem(Constants.SPARK_FLEX_ID);
  public static final VictorSPXSubsystem victorSpxSubsystem = new VictorSPXSubsystem();
  public static final TalonSRXSubsystem talonSrxSubsystem = new TalonSRXSubsystem();
  public static final TalonFXSubsystem talonFxSubsystem = new TalonFXSubsystem();
  // Test sensors
  //public static final ColorSensor colorSensor = new ColorSensor();
  public static final LinearColorEncoder colorEncoder = new LinearColorEncoder();
  // LEDs
  private NewLEDSubsystem ledSubsystem = new NewLEDSubsystem(Constants.LED_PORT);
  //private LEDSubsystem ledSubsystem = new LEDSubsystem(Constants.LED_PORT,Constants.LED_LENGTH);
  // Gyro
  public static AHRS m_gyro = new AHRS(NavXComType.kMXP_SPI);//(SPI.Port.kMXP);
  private final PigeonIMU pidgey = new PigeonIMU(Constants.PIGEON_ID);
  // Servos
  public static ServoSubsystem servoSubsystem = new ServoSubsystem();

  public Robot() {
  }

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // We need to invert one side of the drivetrain so that positive voltages
    // result in both sides moving forward. Depending on how your robot's
    // gearbox is constructed, you might have to invert the left side instead.
    sparkMaxSubsystem.setDefaultCommand(new XboxSparkMaxCommand());
    victorSpxSubsystem.setDefaultCommand(new XboxVictorSPXCommand());
    talonSrxSubsystem.setDefaultCommand(new XboxTalonSRXCommand());
    talonFxSubsystem.setDefaultCommand(new XboxTalonFXCommand());
    ledSubsystem.setTopColor(Color.kGreen);
  }
  
  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run(); 
    // logging
    sparkMaxSubsystem.logToDashboard();
    colorEncoder.logToDashboard();
    victorSpxSubsystem.logToDashboard();
    talonSrxSubsystem.logToDashboard();
    SmartDashboard.putNumber("navX roll", m_gyro.getRoll());
    SmartDashboard.putNumber("navX pitch", m_gyro.getPitch());  
    SmartDashboard.putNumber("navX yaw", m_gyro.getYaw());

    // pigeon
    SmartDashboard.putNumber("pidgey roll", pidgey.getRoll());
    SmartDashboard.putNumber("pidgey pitch", pidgey.getPitch());  
    SmartDashboard.putNumber("pidgey yaw", pidgey.getYaw());
  }

  /** This function is run once each time the robot enters autonomous mode. */
  @Override
  public void autonomousInit() {
    
  }

  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    
  }

  /** This function is called once each time the robot enters teleoperated mode. */
  @Override
  public void teleopInit() {
    configureButtonBindings();
  }

  /** This function is called periodically during teleoperated mode. */
  @Override
  public void teleopPeriodic() {
    // override the led pattern
    //ledSubsystem.setPattern(new AllianceColor()); 
  }
  
  /** This function is called periodically during teleoperated mode. */
  @Override
  public void teleopExit() {
    // override the led pattern
    //ledSubsystem.setPattern(new Rainbow()); 
  }

  /** This function is called once each time the robot enters test mode. */
  @Override
  public void testInit() {
    
    //ledSubsystem.setPattern(new Fire());
  }

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}

  @Override
  public void disabledInit() {
    
  }
  /**
   * Set up the driver inputs.
   */
  private void configureButtonBindings() {
    // First Controller
    driverXbox.a().onTrue(new ButtonVictorSPXCommand().withTimeout(1));
    driverXbox.b().whileTrue(new ServoCommand());
  }
}
