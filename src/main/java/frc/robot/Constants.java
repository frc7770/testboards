package frc.robot;

public class Constants {
        // Spark Max Constants
        public static final int SPARK_MAX_ID = 15;      // CAN ID
        public static final double SPARK_MAX_SPEED = .3;

        // Spark Flex Constants
        public static final int SPARK_FLEX_ID = 52; 
        public static final double SPARK_FLEX_SPEED = .3;

        // CTR motors
        public static final int VICTOR_SPX_ID = 2;
        public static final double VICTOR_SPX_RATIO = 0.85;
        public static final int TALON_SRX_ID = 9;
        public static final int TALON_FX_ID = 12;

        // LED Constants
        public static final int LED_PORT = 1;
        public static final int LED_LENGTH = 73;

        // Sensor Constants
        public static final int PIGEON_ID = 11;

        // Servo Constants
        public static final int SERVO_ID = 0;

}