// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;

import java.util.HashMap;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

/** Add your docs here. */
public class LinearColorEncoder {

    protected final I2C.Port i2cPort = I2C.Port.kOnboard;
    protected ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
    private final ColorMatch m_colorMatcher = new ColorMatch();

    public static enum ReefLevel{LEVEL1, LEVEL2, LEVEL3, o}
    public static HashMap<String, ReefLevel> colorMap = new HashMap<String, ReefLevel>();

    public LinearColorEncoder() {
        addLevelMap(new Color("#425567"), ReefLevel.LEVEL1);
        addLevelMap(new Color("#2d6170"), ReefLevel.LEVEL2);
        addLevelMap(new Color("#8a561e"), ReefLevel.LEVEL3);
    }

    /*
     * Map colors to levels
     */
    private void addLevelMap(Color color,ReefLevel level) {
        colorMap.put(color.toString(),level);
        m_colorMatcher.addColorMatch(color);
    }

    public LinearColorEncoder.ReefLevel getReefLevel() {
        Color detectedColor = m_colorSensor.getColor();
        SmartDashboard.putString("Detected Color ", detectedColor.toString());
        ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);

        // If low confidence return NONE
        SmartDashboard.putNumber("Confidence ", match.confidence);
        if (match.confidence < .95) {
            return ReefLevel.o;
        }

        // if we do not match one of the colors we are looking for return NONE
        SmartDashboard.putString("Match Color ", match.color.toString());
        if(!colorMap.containsKey(match.color.toString())) {
            return ReefLevel.o;
        }

        // return the color match
        return colorMap.get(match.color.toString());
    }

    public void logToDashboard() {
        SmartDashboard.putString("Reef Level ", getReefLevel().name());
    }
}
