// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

/** Add your docs here. */
public class ColorSensor {

    protected final I2C.Port i2cPort = I2C.Port.kOnboard;
    protected ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
    private final ColorMatch m_colorMatcher = new ColorMatch();
    public boolean hasRing = false;
    public int sensorNumber = 0;
    public double scale = 1.15;

    public static enum RingColor{ORANGE, OTHER, NONE}

    public ColorSensor() {
        m_colorMatcher.addColorMatch(Color.kOrange);
        sensorNumber = 1;
    }

    public ColorSensor(Port port) {
        m_colorMatcher.addColorMatch(Color.kOrange);
        m_colorSensor = new ColorSensorV3(port);
        sensorNumber = 2;
    }

    public ColorSensor.RingColor getRing() {
        Color detectedColor = m_colorSensor.getColor();
        ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);
        // If low confidence return none
        SmartDashboard.putNumber("Confidence " + sensorNumber, match.confidence);
        if (match.confidence < .525 * scale) {
            hasRing = false;
            return ColorSensor.RingColor.NONE;
        }

        if(match.color == Color.kOrange && match.confidence > .525 * scale) {
            hasRing = true;
            return ColorSensor.RingColor.ORANGE;
        }
        else 
            hasRing = true;
            return ColorSensor.RingColor.OTHER;
    }

    public void logToDashboard() {
        SmartDashboard.putString("RingColor " + sensorNumber, getRing().name());
        SmartDashboard.putBoolean("Has Ring " + sensorNumber, hasRing);
    }

    public boolean hasRing() {
        return hasRing;
    }
}
