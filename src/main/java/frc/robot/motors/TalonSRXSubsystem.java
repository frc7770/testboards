package frc.robot.motors;

import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TalonSRXSubsystem extends SubsystemBase{
    private TalonSRX talonMotor;

    public TalonSRXSubsystem() {
        talonMotor = new TalonSRX(Constants.TALON_SRX_ID);
    }

    public void setMainSpeed(double speed){
        talonMotor.set(TalonSRXControlMode.PercentOutput, speed);
    }

    public void stop() {
        talonMotor.set(TalonSRXControlMode.PercentOutput, 0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Talon SRX Motor Voltage ", talonMotor.getBusVoltage());
    }

}
