package frc.robot.motors;

import com.revrobotics.spark.SparkBase.PersistMode;
import com.revrobotics.spark.SparkBase.ResetMode;
import com.revrobotics.spark.SparkFlex;
import com.revrobotics.spark.SparkLowLevel;
import com.revrobotics.spark.config.SparkBaseConfig.IdleMode;
import com.revrobotics.spark.config.SparkFlexConfig;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class SparkFlexSubsystem extends SubsystemBase{
    private RelativeEncoder sparkFlexEncoder;
    private SparkFlex sparkFlex;

    public SparkFlexSubsystem(int canId) {
        sparkFlex = new SparkFlex(canId, SparkLowLevel.MotorType.kBrushless);
        sparkFlexEncoder = sparkFlex.getEncoder();
        sparkFlexEncoder.setPosition(0);
        // 2024 obsolete sparkFlex.setIdleMode(IdleMode.kBrake);
        SparkFlexConfig config = new SparkFlexConfig();

        config
            //.inverted(true)
            .idleMode(IdleMode.kBrake);
        //config.encoder
        //    .positionConversionFactor(1000)
        //    .velocityConversionFactor(1000);
        //config.closedLoop
        //    .feedbackSensor(FeedbackSensor.kPrimaryEncoder)
        //    .pid(1.0, 0.0, 0.0);
            
        sparkFlex.configure(config, ResetMode.kResetSafeParameters, PersistMode.kPersistParameters);
    }

    public void setMainSpeed(double speed){
        sparkFlex.set(speed * Constants.SPARK_MAX_SPEED);
    }

    public void stop() {
        sparkFlex.set(0);
    }

    public double getMainEncoderAngle() {
        return sparkFlexEncoder.getPosition(); // need to do some math here * Constants.ARM_MAIN_ENCODER_CONVERSION;
    }

    public double getMainEncoderPosition() {
        return sparkFlexEncoder.getPosition();
    }

    public void zeroEncoders() {
        sparkFlexEncoder.setPosition(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Spark Flex Motor Speed ", sparkFlexEncoder.getVelocity());
        SmartDashboard.putNumber("Spark Flex Encoder Position ", getMainEncoderPosition());
    }

}
