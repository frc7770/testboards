package frc.robot.motors;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Robot;

public class XboxSparkMaxCommand extends Command {
    CommandXboxController controller;
    
    public XboxSparkMaxCommand() {
        addRequirements(Robot.sparkMaxSubsystem);
        controller = Robot.driverXbox;
    }

    @Override
    public void execute() {
        double speed = controller.getRightY();
        Robot.sparkMaxSubsystem.setMainSpeed(speed);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.sparkMaxSubsystem.stop();
    }
}
