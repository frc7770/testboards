package frc.robot.motors;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Robot;

public class XboxVictorSPXCommand extends Command {
    CommandXboxController controller;
    
    public XboxVictorSPXCommand() {
        addRequirements(Robot.victorSpxSubsystem);
        controller = Robot.driverXbox;
    }

    @Override
    public void execute() {
        double speed = controller.getLeftY();
        Robot.victorSpxSubsystem.setMainSpeed(speed);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.victorSpxSubsystem.stop();
    }
}
