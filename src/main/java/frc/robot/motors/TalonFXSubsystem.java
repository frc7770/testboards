package frc.robot.motors;


import com.ctre.phoenix6.hardware.TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TalonFXSubsystem extends SubsystemBase{
    private TalonFX talonMotor;

    public TalonFXSubsystem() {
        talonMotor = new TalonFX(Constants.TALON_FX_ID);
    }

    public void setMainSpeed(double speed){
        talonMotor.set(speed);
    }

    public void stop() {
        talonMotor.set(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Talon FX Motor Voltage ", talonMotor.getMotorVoltage().getValueAsDouble());
    }

}
