package frc.robot.motors;

import com.revrobotics.spark.SparkBase.PersistMode;
import com.revrobotics.spark.SparkBase.ResetMode;
import com.revrobotics.spark.SparkLowLevel;
import com.revrobotics.spark.SparkMax;
import com.revrobotics.spark.config.ClosedLoopConfig.FeedbackSensor;
import com.revrobotics.spark.config.SparkBaseConfig.IdleMode;
import com.revrobotics.spark.config.SparkMaxConfig;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class SparkMaxSubsystem extends SubsystemBase{
    private RelativeEncoder sparkMaxEncoder;
    private SparkMax sparkMax;

    public SparkMaxSubsystem() {
        sparkMax = new SparkMax(Constants.SPARK_MAX_ID, SparkLowLevel.MotorType.kBrushless);
        sparkMaxEncoder = sparkMax.getEncoder();
        sparkMaxEncoder.setPosition(0);
        // 2024 obsolete sparkMax.setIdleMode(IdleMode.kBrake);
        SparkMaxConfig config = new SparkMaxConfig();

        config
            //.inverted(true)
            .idleMode(IdleMode.kBrake);
        //config.encoder
        //    .positionConversionFactor(1000)
        //    .velocityConversionFactor(1000);
        //config.closedLoop
        //    .feedbackSensor(FeedbackSensor.kPrimaryEncoder)
        //    .pid(1.0, 0.0, 0.0);
            
        sparkMax.configure(config, ResetMode.kResetSafeParameters, PersistMode.kPersistParameters);
    }

    public void setMainSpeed(double speed){
        sparkMax.set(speed * Constants.SPARK_MAX_SPEED);
    }

    public void stop() {
        sparkMax.set(0);
    }

    public double getMainEncoderAngle() {
        return sparkMaxEncoder.getPosition(); // need to do some math here * Constants.ARM_MAIN_ENCODER_CONVERSION;
    }

    public double getMainEncoderPosition() {
        return sparkMaxEncoder.getPosition();
    }

    public void zeroEncoders() {
        sparkMaxEncoder.setPosition(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Spark Max Motor Speed ", sparkMaxEncoder.getVelocity());
        SmartDashboard.putNumber("Spark Max Encoder Position ", getMainEncoderPosition());
    }

}
