package frc.robot.motors;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Robot;

public class XboxTalonFXCommand extends Command {
    CommandXboxController controller;
    
    public XboxTalonFXCommand() {
        addRequirements(Robot.talonFxSubsystem);
        controller = Robot.driverXbox;
    }

    @Override
    public void execute() {
        double speed = controller.getLeftX();
        Robot.talonFxSubsystem.setMainSpeed(speed);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.talonFxSubsystem.stop();
    }
}
