package frc.robot.motors;

import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class VictorSPXSubsystem extends SubsystemBase{
    private VictorSPX victorMotor;

    public VictorSPXSubsystem() {
        victorMotor = new VictorSPX(Constants.VICTOR_SPX_ID);
    }

    public void setMainSpeed(double speed){
        victorMotor.set(VictorSPXControlMode.PercentOutput, speed * Constants.VICTOR_SPX_RATIO);
    }

    public void stop() {
        victorMotor.set(VictorSPXControlMode.PercentOutput, 0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Victor SPX Motor Voltage ", victorMotor.getBusVoltage());
    }

}
