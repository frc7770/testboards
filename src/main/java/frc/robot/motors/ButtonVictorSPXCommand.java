package frc.robot.motors;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Robot;


// turn on shooter and set to certain speed
public class ButtonVictorSPXCommand extends Command{
  
  public ButtonVictorSPXCommand() {
    addRequirements(Robot.victorSpxSubsystem);
  } 

  public void execute(){
    Robot.victorSpxSubsystem.setMainSpeed(1.0);

  }
  
  @Override
  public void end(boolean interrupted){
    Robot.victorSpxSubsystem.stop();
  }
}
