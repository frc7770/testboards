package frc.robot.leds;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import java.util.Random;

public class NewLEDSubsystem extends SubsystemBase {
  // LED strip configuration
  private final AddressableLED m_led;
  private final AddressableLEDBuffer m_ledBuffer;
  private final int TOTAL_LENGTH = 73;
  private final int SIDE_SECTION_LENGTH = 48;
  private final int TOP_SECTION_LENGTH = 25;
  
  // Random number generator for fire effect
  private final Random random = new Random();

  private Color m_topColor = null;
  
  // Fire effect parameters
  private int cooling = 55;      // Cooling rate - higher values cool faster
  private int sparking = 120;    // Sparking rate - higher values create more "sparks"
  private int[] heat;            // Array to track "heat" of each LED
  
  // Rainbow pattern variables
  private int rainbowFirstPixelHue = 0;
  private int rainbowStep = 3;
  
  public NewLEDSubsystem(int port) {
    // Initialize LED strip
    m_led = new AddressableLED(port);
    m_ledBuffer = new AddressableLEDBuffer(TOTAL_LENGTH);
    m_led.setLength(TOTAL_LENGTH);
    
    // Initialize heat array for fire effect
    heat = new int[SIDE_SECTION_LENGTH];
    
    // Start the LED output
    m_led.start();
  }
  
  @Override
  public void periodic() {
    // Update the patterns
    updateFireEffect();
    if(m_topColor==null) {
      updateRainbow();
    } else {
      updateTopColor();
    }
    
    // Send the data to the LED strip
    m_led.setData(m_ledBuffer);
  }
  
  private void updateFireEffect() {
    // Step 1: Cool each cell by a random amount
    for (int i = 0; i < SIDE_SECTION_LENGTH; i++) {
      int cooldown = random.nextInt(((cooling * 10) / SIDE_SECTION_LENGTH) + 2);
      
      if (cooldown > heat[i]) {
        heat[i] = 0;
      } else {
        heat[i] = heat[i] - cooldown;
      }
    }
    
    // Step 2: Heat rises - "pixel data" drifts upward
    for (int i = SIDE_SECTION_LENGTH - 1; i >= 2; i--) {
      heat[i] = (heat[i - 1] + heat[i - 2] + heat[i - 2]) / 3;
    }
    
    // Step 3: Randomly ignite new "sparks" near the bottom
    if (random.nextInt(255) < sparking) {
      int y = random.nextInt(7);
      heat[y] = Math.min(heat[y] + random.nextInt(160) + 95, 255);
    }
    
    // Step 4: Convert heat to LED colors
    for (int i = 0; i < SIDE_SECTION_LENGTH; i++) {
      setFireColorFromHeat(i, heat[i]);
    }
  }
  
  private void setFireColorFromHeat(int pixelIndex, int heat) {
    // Scale heat down to 0-255 range if needed
    heat = Math.min(heat, 255);
    
    // Heatmap colors: black -> red -> orange -> yellow -> white
    int r, g, b;
    
    if(DriverStation.getAlliance().get().equals(Alliance.Red)) {
      // RED
      if (heat < 85) {  // Black to Red
        r = heat * 3;
        g = 0;
        b = 0;
      } else if (heat < 170) {  // Red to Orange/Yellow
        r = 255;
        g = (heat - 85) * 3;
        b = 0;
      } else {  // Yellow to White
        r = 255;
        g = 255;
        b = (heat - 170) * 3;
      }
    } else {
      // BLUE
      if (heat < 85) {  // Black to Blue
        r = 0;
        g = 0;
        b = heat * 3;
      } else if (heat < 170) {  // blue to green/Yellow
        r = 0;
        g = (heat - 85) * 3;
        b = 255;
      } else {  // Yellow to White
        r = 255;
        g = 255;
        b = (heat - 170) * 3;
      }
    }
    
    // Set the color for this LED
    m_ledBuffer.setRGB(pixelIndex, r, g, b);
  }
  
  private void updateRainbow() {
    // Update the rainbow pattern
    rainbowFirstPixelHue += rainbowStep;
    rainbowFirstPixelHue %= 180; // Wrap around at 180
    
    // Apply rainbow to second section (FIRE_SECTION_LENGTH to TOTAL_LENGTH - 1)
    for (int i = 0; i < TOP_SECTION_LENGTH; i++) {
      // Calculate the hue - spread the spectrum across the LEDs
      final int hue = (rainbowFirstPixelHue + (i * 180 / TOP_SECTION_LENGTH)) % 180;
      
      // Set the value at the target pixel
      int ledIndex = SIDE_SECTION_LENGTH + i;
      m_ledBuffer.setHSV(ledIndex, hue, 255, 128);
    }
  }

  private void updateTopColor() {
    // Apply rainbow to second section (FIRE_SECTION_LENGTH to TOTAL_LENGTH - 1)
    for (int i = 0; i < TOP_SECTION_LENGTH; i++) {
      
      // Set the value at the target pixel
      int ledIndex = SIDE_SECTION_LENGTH + i;
      m_ledBuffer.setLED(ledIndex, m_topColor);
    }
  }
  
  // Fire control methods
  public void setFireCooling(int cooling) {
    // Adjust how quickly the fire cools down
    // Lower values = hotter, more persistent flames
    // Higher values = cooler, more random flames
    // Typical values: 20 (hot) to 100 (cool)
    this.cooling = Math.max(20, Math.min(100, cooling));
  }
  
  public void setFireSparking(int sparking) {
    // Adjust how many new sparks are created at the bottom
    // Higher values = more active fire
    // Typical values: 50 (mild) to 200 (intense)
    this.sparking = Math.max(50, Math.min(200, sparking));
  }
  
  // Rainbow control methods
  public void setRainbowSpeed(int speed) {
    // Adjust the rainbow cycling speed (higher = faster)
    // Typical values: 1 (slow) to 5 (fast)
    this.rainbowStep = Math.max(1, Math.min(5, speed));
  }
  
  public void stop() {
    // Turn off all LEDs
    for (int i = 0; i < TOTAL_LENGTH; i++) {
      m_ledBuffer.setRGB(i, 0, 0, 0);
    }
    m_led.setData(m_ledBuffer);
  }

  public void setTopColor(Color color) {
    m_topColor=color;
  }

}