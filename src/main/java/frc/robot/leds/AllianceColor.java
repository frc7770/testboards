package frc.robot.leds;

import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.util.Color;

public class AllianceColor implements PatternInterface {
    private int m_rainbowFirstPixelHue;
    private int counter = 0;

    @Override
    public void resetLEDs(AddressableLEDBuffer m_ledBuffer) {
        // get alliance
        Color color = Color.kRed;
        if(DriverStation.getAlliance().get().equals(Alliance.Blue)) {
            color = Color.kBlue;
        }
        // For every pixel
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            // Set the value
            m_ledBuffer.setLED(i, color);
        }
        // Increase by to make the rainbow "move"
        m_rainbowFirstPixelHue += 3;
        // Check bounds
        m_rainbowFirstPixelHue %= 180;
    }
    
}
