package frc.robot.leds;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LEDSubsystem extends SubsystemBase {
    private AddressableLED m_led;
    private AddressableLEDBuffer m_ledBuffer;
    private int m_rainbowFirstPixelHue;
    private int counter = 0;
    private int length = 110;
    private PatternInterface pattern = new Rainbow();

    public LEDSubsystem(int port, int length) {
        // PWM port 9
        // Must be a PWM header, not MXP or DIO
        m_led = new AddressableLED(port);
        // init length
        this.length=length;

        // Reuse buffer
        // Default to a length of 60, start empty output
        // Length is expensive to set, so only set it once, then just update data
        m_ledBuffer = new AddressableLEDBuffer(this.length);
        m_led.setLength(m_ledBuffer.getLength());

        // Set the data
        pattern.resetLEDs(m_ledBuffer);
        m_led.setData(m_ledBuffer);
        m_led.start();
    }

    public void setPattern(PatternInterface pattern) {
        this.pattern=pattern;
    }

    @Override
    public void periodic() {
        // loop the pattern
        pattern.resetLEDs(m_ledBuffer);
        // Set the LEDs
        m_led.setData(m_ledBuffer);
        // increment counter
        counter = Math.floorMod(counter++,length);
    }

}