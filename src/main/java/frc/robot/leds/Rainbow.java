package frc.robot.leds;

import edu.wpi.first.wpilibj.AddressableLEDBuffer;

public class Rainbow implements PatternInterface {
    private int m_rainbowFirstPixelHue;
    private int counter = 0;

    @Override
    public void resetLEDs(AddressableLEDBuffer m_ledBuffer) {
        // For every pixel
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            // Calculate the hue - hue is easier for rainbows because the color
            // shape is a circle so only one value needs to precess
            final var hue = (m_rainbowFirstPixelHue + ((i+counter) * 180 / m_ledBuffer.getLength())) % 180;
            // Set the value
            m_ledBuffer.setHSV(i+counter, hue, 255, 128);
        }
        // Increase by to make the rainbow "move"
        m_rainbowFirstPixelHue += 3;
        // Check bounds
        m_rainbowFirstPixelHue %= 180;
    }
    
}
