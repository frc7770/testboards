package frc.robot.leds;

import edu.wpi.first.wpilibj.AddressableLEDBuffer;

public interface PatternInterface {

    public void resetLEDs(AddressableLEDBuffer m_ledBuffer);
    
}
