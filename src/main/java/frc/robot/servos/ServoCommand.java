package frc.robot.servos;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Robot;

public class ServoCommand extends Command {
    CommandXboxController controller;
    
    public ServoCommand() {
        addRequirements(Robot.servoSubsystem);
        controller = Robot.driverXbox;
    }

    @Override
    public void execute() {
        Robot.servoSubsystem.setAngle(270);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted){
        Robot.servoSubsystem.setAngle(0);
    }
}
