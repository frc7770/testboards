package frc.robot.servos;

import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ServoSubsystem extends SubsystemBase{
    private static Servo servo;

    public ServoSubsystem() {
        servo = new Servo(Constants.SERVO_ID);
        servo.setAngle(0);
    }

    public double getAngle() {
        return servo.getAngle();
    }

    public void setAngle(double angle) {
        servo.setAngle(angle);
    }
}
